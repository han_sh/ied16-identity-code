/* 
  Students sometimes collaborated
  — which is to say: they functioned and drank together —
  and hence were formed images of their collaborative work
*/
  
// Kevin was a master of button-pushing:
// he knew what buttons to push and how,
// to help his students learn and enjoy their work.
void keyPressed() {
  // When given SPACE and a BAR,
  if (key == ' ') {
    // students blended their skills together
    // to concoct beautiful experiences.
    BLENDwork();
  }

  /*
    SSSSSSome times, they SAVED their work
    so that they could share it with 
    the land that Kevin, the Walker, walked on.
  */
  if (key == 's' || key == 'S') {
    save(   "data/output/"
          + "IEDBlend-"
          + timestamp()
          //+ ".jpg" // commenting this out will save TIFFs
        );
  }
}

/*
  In a collaboration,
  students blended their strengths 
  to invent beautiful experiences.
*/
void BLENDwork(){
  // they began ideating on a blank board,
  background(255);
  // huddled up together,
  for(int i=0; i<graduate.length; i++) {
    // shuffled about randomly,
    int interestedStudent = (int)random(graduate.length);
    // and, one at a time, added work to the concoction
    graduate[interestedStudent].blendWork();
  }
}