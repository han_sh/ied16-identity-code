/* 
  Kevin created GLOBAL environment,
  filled with things that varied and things that didn't,
  so that he could build his world with that.
*/
String [][] studentData = { // First Name, Image.extn
                         {"Jean",   "Jean.png"}
                        ,{"Maximo", "Maximo.png"}
                        ,{"Jelka",  "Jelka.png"}
                        ,{"Sam",    "Sam.png"}
                        ,{"Tom",    "Tom.png"}
                        ,{"Hayden", "Hayden.png"}
                        ,{"Tess",   "Tess.png"}
                        ,{"Meret",  "Meret.png"}
                        ,{"Kelly",  "Kelly.png"}
                        ,{"Joanne", "Joanne.png"}
                      };