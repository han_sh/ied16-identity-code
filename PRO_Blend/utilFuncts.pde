String timestamp() {
  return (   normaliseTime(year()) 
           + normaliseTime(month()) 
           + normaliseTime(day()) 
           + normaliseTime(hour())
           + normaliseTime(minute())
           + normaliseTime(second())
         );
  } String normaliseTime(int i) {
      String I = ""+i;
      if(i>=10) return I;
      else return "0"+I;
}