/*
  A walking Walker once,
  Walked the land.
  Kevin was his name,
  Information, his game.
  Kevin wanted to gift  
  Experiences, bold & new.
  So Kevin did design,
  Information Experience Design.
  In 2016, his third creation
  Will come of age.
  This is a story of all
  They have, on this stage.
*/

// First, Kevin spawned the
class ofIED2016 {
  
/*
  Kevin DECLARED to the world,
  that each student shall have a name and an image.
  Kevin also clarified that the students 
  shall not be made in his own image.
  Each shall be unique and look rather funny.
*/
  String name;
  PImage image;
  
/*
  Kevin CONSTRUCTed each student,
  initiating each student with a name
  and a super-power to create an image.
*/
  ofIED2016(int studentID) {
    name = studentData[studentID][0];
    image = makeImage(studentID);
  }
  
/*
  Kevin gave his students
  FUNCTIONing super-powers
  to make awesome things:
*/
  
  // Students functioned and frolicked and worked and partied some more,
  // and that is how formed images of their work.
  PImage makeImage(int studentID) {
    return loadImage("projectImages/"+studentData[studentID][1]);
  }
  
  // Each student was different,
  // but they knew how to work with each other,
  void blendWork() {
    blend(image, 
          0, 0, image.width, image.height, 
          0, 0, width, height, 
          // because they celebrated their collective differences.
          DIFFERENCE);
  }
}